/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    audioDeviceManager.setDefaultMidiOutput("SimpleSynth virtual input");

    midiLabel.setText("SetText", dontSendNotification);
    addAndMakeVisible(&midiLabel);
    
    messageType.addItem("Note", 1);
    addAndMakeVisible(&messageType);
    
    channelNumber.setSliderStyle(Slider:: IncDecButtons);
    channelNumber.setRange(0, 1, 1);
    channelNumber.addListener(this);
    addAndMakeVisible(&channelNumber);
    
    noteNumber.setSliderStyle(Slider:: IncDecButtons);
    noteNumber.setRange(0, 127, 1);
    noteNumber.addListener(this);
    addAndMakeVisible(&noteNumber);
    
    channelVelocity.setSliderStyle(Slider:: IncDecButtons);
    channelVelocity.setRange(0, 127, 1);
    channelVelocity.addListener(this);
    addAndMakeVisible(&channelVelocity);
    
    send.setButtonText("SEND");
    send.addListener(this);
    addAndMakeVisible(&send);
    
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

void MainComponent::resized()
{
  //midiLabel.setBounds(50, 10, getWidth() -20, 40);
    
    messageType.setBounds(10, 10, getWidth() -410, 30);
    channelNumber.setBounds(100, 10, getWidth() -400, 30);
    noteNumber.setBounds(200, 10, getWidth() -400, 30);
    channelVelocity.setBounds(300, 10, getWidth() -400, 30);
    
    send.setBounds(400, 10, getWidth()-400, 30);
    
}



void MainComponent:: sliderValueChanged (Slider* slider)
{
    //DBG("Slider Change");
    
  
    /*DBG("ChannelNumber:" << channelNumber.getValue() << "\n");
    DBG("NoteNumber:" << noteNumber.getValue() << "\n");
    DBG("ChannelVelcoity:" << channelVelocity.getValue() << "\n");*/
    
    ChannelNumberRecived = channelNumber.getValue();
    NoteNumberRecived = noteNumber.getValue();
    ChannelVelocityRecived = channelVelocity.getValue();
    
}

void MainComponent:: buttonClicked(Button* button)
{
   
    
    MidiMessage message = MidiMessage::noteOn(ChannelNumberRecived, NoteNumberRecived, (uint8)ChannelVelocityRecived);
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(message);
    
    message = MidiMessage::noteOff(ChannelNumberRecived, NoteNumberRecived);
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(message);
    
}

void MainComponent:: handleIncomingMidiMessage (MidiInput* source,
                                                const MidiMessage& message)
{
    //DBG("Midi Message Recived");
    
    String midiText;
    
    
        midiText << "NoteOn: Channel" << message.getChannel();
        midiText << ":Number" << message.getNoteNumber();
        midiText << ":Velocity" << message.getVelocity();
    
    
    //Pitch Wheel
    if (message.isPitchWheel())
    {
        midiText << "\n Pitch Wheel:" << message.getPitchWheelValue();
    }
    
    //AfterTouch
    if (message.isChannelPressure())
    {
        midiText << "\n Aftertouch:" << message.getChannelPressureValue();
    }
    //Controller
    if (message.isController())
    {
       midiText << "Controller Number:" << message.getControllerNumber();
       midiText << "Controller Value:" << message.getControllerValue();
    }
    
    
    
    
    midiLabel.getTextValue() = midiText;
    
    
    
}