/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component, public MidiInputCallback, public Button:: Listener, public Slider:: Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized();
    void buttonClicked(Button* button) override;
    void sliderValueChanged (Slider* slider) override;
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
    
    AudioDeviceManager audioDeviceManager;
    Label midiLabel;
    
    ComboBox messageType;
    Slider channelNumber;
    Slider noteNumber;
    Slider channelVelocity;
    TextButton send;
    
    int ChannelNumberRecived;
    int NoteNumberRecived;
    int ChannelVelocityRecived;
    
};


#endif  // MAINCOMPONENT_H_INCLUDED
